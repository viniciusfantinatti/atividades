package com.itau;

public class Main {

    public static void main(String[] args) {
        Investimento investimento = new Investimento();
        TelaTerminal telaTerminal = new TelaTerminal();
        Simulador simulador = new Simulador();

        investimento = telaTerminal.entradaDados();
        telaTerminal.retornaDados();
    }
}
