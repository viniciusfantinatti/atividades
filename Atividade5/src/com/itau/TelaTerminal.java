package com.itau;

import java.util.Scanner;

public class TelaTerminal {

    Scanner scanner = new Scanner(System.in);
    Investimento investimento = new Investimento();
    Simulador simulador = new Simulador();



    public Investimento entradaDados(){

        System.out.println("Bem vindo ao simulador de Investimentos!");
        System.out.println(" ");
        System.out.print("Digite o valor a ser investido: R$");
        investimento.setValorInvestido(scanner.nextDouble());
        System.out.print("Digite o tempo que deseja investir (em meses): ");
        investimento.setTempo(scanner.nextInt());
//        System.out.print("Digite a taxa de remuneração: ");
//        investimento.setTaxaRemuneracao(scanner.nextDouble());

        return investimento;
    }

    public void retornaDados(){
        System.out.println("O total de lucro que você terá é de: R$" + simulador.calcular(investimento));
    }
}
