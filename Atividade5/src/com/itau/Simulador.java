package com.itau;

public class Simulador {
    private double calculo;
    private double resultado;

    public double calcular(Investimento investimento){
        calculo = investimento.getValorInvestido() * (investimento.getTaxaRemuneracao() / 100);
        resultado =  calculo * investimento.getTempo();

        return resultado;
    }
}
