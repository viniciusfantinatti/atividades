package com.itau;

public class Investimento {

    private double valorInvestido;
    private int tempo;
    private double taxaRemuneracao = 0.7D;
//    private double taxaRemuneracao;

    public Investimento(){
    }

    public Investimento(double valorInvestido, int tempo, double taxaRemuneracao){
        this.valorInvestido = valorInvestido;
        this.tempo = tempo;
        this.taxaRemuneracao = taxaRemuneracao;
    }

    public Investimento(double valorInvestido, int tempo){
        this.valorInvestido = valorInvestido;
        this.tempo = tempo;
    }

    public double getValorInvestido() {
        return valorInvestido;
    }

    public int getTempo() {
        return tempo;
    }

    public double getTaxaRemuneracao() {
        return taxaRemuneracao;
    }

    public void setValorInvestido(double valorInvestido) {
        this.valorInvestido = valorInvestido;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }

    public void setTaxaRemuneracao(double taxaRemuneracao) {
        this.taxaRemuneracao = taxaRemuneracao;
    }
}
