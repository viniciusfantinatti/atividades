package br.com.itau;

public class Carta {
    private Naipe naipe;
    private NumeroCarta numeroCarta;

    public Carta(Naipe naipe, NumeroCarta numeroCarta) {
        this.naipe = naipe;
        this.numeroCarta = numeroCarta;
    }

    public Naipe getNaipe() {
        return naipe;
    }

    public void setNaipe(Naipe naipe) {
        this.naipe = naipe;
    }

    public NumeroCarta getNumeroCarta() {
        return numeroCarta;
    }

    public void setNumeroCarta(NumeroCarta numeroCarta) {
        this.numeroCarta = numeroCarta;
    }
}
