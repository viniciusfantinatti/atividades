package br.com.itau;

public enum Naipe {
    OURO,
    ESPADA,
    COPAS,
    PAUS;
}
